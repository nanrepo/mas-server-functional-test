# MAS-SERVER-FUNCTIONAL-TEST

[![N|nan](https://nantech.com.ar/wp-content/uploads/2020/03/logo.png)](https://nantech.com.ar/)


Libreria de automation para la validación de los endpoints de graphql para SearchMAS.

Users story cubiertas:
   - [QAS-15]
   - [QAS-16]
   - [QAS-17]
   - [QAS-18]
   - [QAS-21]
   - [QAS-24]

# New Features!

  - Se agrega el tenant mockeado de demo@arrowsoluciones.com.ar



La libreria se puede importar y ejecutar separada del proyecto o integrarlo con el comando *yarn add git clone https://GastonGenaud@bitbucket.org/nanrepo/mas-server-functional-test.git* dentro de las dependencias.

Comandos para ejecutar la librería:

```
 yarn start            =>   inicializa la base de datos
 yarn test:all         =>   ejecuta todos los test suite de las users story HOME, LOGIN, BOARDING, WORKSPACE y ADS
 yarn SUITE_FOLDER     =>   los suites folders son suitehome, suitelogin, suiteboarding, suiteworkspace y suiteads
 yarn initDB           =>   despierta la base de datos en caso para la primera ejecución
```

Usage: just-api [options] [files]
```

Options:

    -V, --version                       outputs the version number
    --parallel <integer>                specify the number of suites to be run in parallel
    --reporter <reporternames>          specify the reporters to use, comma separated list e.g json,html
    --reporter-options <k=v,k2=v2,...>  reporter-specific options
    --grep <pattern>                    only run tests matching <pattern/string>
    --recursive                         include sub directories when searching for suites
    --reporters                         display available reporters
    -h, --help                          outputs usage information
```

### STACK

* JUST-API 
* MOCHA 
* CHAI 
* mongodb-memory-server 
* MONGOOSE]
* MONGODB 
* graphql 
* EXPRESS 



### Installation



```
$ git clone git clone https://GastonGenaud@bitbucket.org/nanrepo/mas-server-functional-test.git
$ yarn
$ -----ON REPOSITORY------------
$ yarn add git clone https://GastonGenaud@bitbucket.org/nanrepo/mas-server-functional-test.git
```

License
----
© 2020 NaN – All rights reserved



   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
   [QAS-15]:<https://nandigital.atlassian.net/browse/QAS-15>
   [QAS-16]: <https://nandigital.atlassian.net/browse/QAS-16>
   [QAS-17]: <https://nandigital.atlassian.net/browse/QAS-17>
   [QAS-18]: <https://nandigital.atlassian.net/browse/QAS-18>
   [QAS-21]: <https://nandigital.atlassian.net/browse/QAS-21>
   [QAS-24]: <https://nandigital.atlassian.net/browse/QAS-24>
