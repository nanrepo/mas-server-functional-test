const app = require('./server');

const port = process.env.PORT || '4000';

app.listen(port);

console.log(`Server ready at http://localhost:${port}/graphql`);
console.log('playground check : http://localhost:4000/playground');
console.log('database on memory:');
console.log('mas-gastongenaudar@gmail.com');
console.log('mas-demo_arrowsoluciones_com_ar');
console.log('global-config');

function exitMongoMemory(arg) {
    console.log(`Se apaga la base de datos de mongo memory server code : ${arg}`);
    process.exit()

  }
  
//setTimeout(exitMongoMemory, 300000, 'SUCCESS');