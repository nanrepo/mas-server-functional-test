const resolvers = {
    locations: async (_, context) => {
        const { db } = await context();
        return db
            .collection('locations')
            .find()
            .toArray();
    },
    location: async ({ id }, context) => {
        const { db } = await context();

        return db.collection('locations').findOne({ id });
    },
};

module.exports = resolvers;
