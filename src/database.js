const { MongoMemoryServer } = require('mongodb-memory-server');
// eslint-disable-next-line import/no-unresolved
const { MongoClient } = require('mongodb');
//Archivos con la data mockeada
const data = require('./data/global_config');
const dataDemo = require('./data/tenantDemo');
const dataInfo = require('./data/tenantInfo');
const dataCmUrs = require('./data/common_users');
const dataJnieto = require ('./data/tenantJnieto');
const dataBrandify = require('./data/brandify');

const mongoTenant = new MongoMemoryServer({
    instance: {
        port: 65261,
        dbName: 'mas-jnieto_where2getit_com',
        dbPath: './src',
        binary: { version: 'v4.2-latest' }
    },
    binary: {
        downloadDir: `${process.cwd()}/mongodb-binaries`,
    },
    autoStart: true,
});

async function startDatabase() {
    const mongoUriTenant = await mongoTenant.getUri();
    //Crea las bases de datos dentro del cluster local
    const connTenantJnieto = await MongoClient.connect(mongoUriTenant, {useNewUrlParser: true,useUnifiedTopology: true,});
    const connGlobalConfig = await MongoClient.connect('mongodb://127.0.0.1:65261/global-config', { useUnifiedTopology: true });
    const connTenantDemo = await MongoClient.connect('mongodb://127.0.0.1:65261/mas-demo_arrowsoluciones_com_ar', { useUnifiedTopology: true });
    const connTenantInfo = await MongoClient.connect('mongodb://127.0.0.1:65261/mas-info_arrowsoluciones_com_ar', { useUnifiedTopology: true });
    const connCommonUser = await MongoClient.connect('mongodb://127.0.0.1:65261/mas-common_users', { useUnifiedTopology: true });
    const connBrandify = await MongoClient.connect('mongodb://127.0.0.1:65261/mas-brandify', { useUnifiedTopology: true });


    // Inserta las colecciones y la data dentro de las bses de datos.
    const configDb = connGlobalConfig.db();
    await configDb.collection('accounts').insertMany(data.accounts);
    await configDb.collection('addressmap').insertMany(data.addressmap);
    await configDb.collection('adminconfigs').insertMany(data.adminconfigs);
    await configDb.collection('adminemails').insertMany(data.adminemails);
    await configDb.collection('adsaccountrequests').insertMany(data.adsaccountrequests);
    await configDb.collection('authtokens').insertMany(data.authtokens);
    await configDb.collection('capabilities').insertMany(data.capabilities);
    await configDb.collection('clients').insertMany(data.clients);
    await configDb.collection('configs').insertMany(data.configs);
    await configDb.collection('connectors').insertMany(data.connectors);
    await configDb.collection('entities').insertMany(data.entities);
    await configDb.collection('externalaccounts').insertMany(data.externalaccounts);
    await configDb.collection('gmbclients').insertMany(data.gmbclients);
    await configDb.collection('gmbverticals').insertMany(data.gmbverticals);
    await configDb.collection('roles').insertMany(data.roles);
    await configDb.collection('users').insertMany(data.users);

    const brandifyDb = connBrandify.db();
    await brandifyDb.collection('extractjobs').insertMany(dataBrandify.extractjobs);
    await brandifyDb.collection('members').insertMany(dataBrandify.members);
    await brandifyDb.collection('rankingjobs').insertMany(dataBrandify.rankingjobs);
    await brandifyDb.collection('rankings').insertMany(dataBrandify.rankings);
    await brandifyDb.collection('suggestions').insertMany(dataBrandify.suggestions);

    const tenantDemoDb = connTenantDemo.db();
    await tenantDemoDb.collection('users').insertMany(dataDemo.users);

    const tenantDb = connTenantJnieto.db();
    await tenantDb.collection('users').insertMany(dataJnieto.users);
    
    const tenantInfoDb = connTenantInfo.db();
    await tenantInfoDb.collection('users').insertMany(dataInfo.users);

    const common_usersDb = connCommonUser.db();
    await common_usersDb.collection('addTemplate').insertMany(dataCmUrs.addTemplate);
    await common_usersDb.collection('currencies').insertMany(dataCmUrs.currencies);
    await common_usersDb.collection('extractjobs').insertMany(dataCmUrs.extractjobs);
    await common_usersDb.collection('files').insertMany(dataCmUrs.files);
    await common_usersDb.collection('googleposts').insertMany(dataCmUrs.googleposts);
    await common_usersDb.collection('googlereviews').insertMany(dataCmUrs.googlereviews);
    await common_usersDb.collection('keywordplans').insertMany(dataCmUrs.keywordplans);
    await common_usersDb.collection('localposts').insertMany(dataCmUrs.localposts);
    await common_usersDb.collection('locations').insertMany(dataCmUrs.locations);
    await common_usersDb.collection('locationads').insertMany(dataCmUrs.locationads);
    await common_usersDb.collection('members').insertMany(dataCmUrs.members);
    await common_usersDb.collection('posttemplates').insertMany(dataCmUrs.posttemplates);
    await common_usersDb.collection('radius').insertMany(dataCmUrs.radius);
    await common_usersDb.collection('rankingjobs').insertMany(dataCmUrs.rankingjobs);
    await common_usersDb.collection('rankings').insertMany(dataCmUrs.rankings);
    await common_usersDb.collection('suggestions').insertMany(dataCmUrs.suggestions);
    await common_usersDb.collection('workspaces').insertMany(dataCmUrs.workspaces);





}

// eslint-disable-next-line no-unused-vars
const db = startDatabase();

async function stopDatabase() {
    await mongoTenant.stop();
}

module.exports = {
    startDatabase,
    stopDatabase,
};
