const { buildSchema } = require('graphql');

const schema = buildSchema(`
    type Query {
        locations: [Location!]!,
        location(id: String!): Location
    }

    type Location {
        _id: String!
        id: String
        account: String
        languageCode: String
        locationName: String
        name: String
        primaryPhone: String
        websiteUrl: String
        
    }
`);

module.exports = schema;
