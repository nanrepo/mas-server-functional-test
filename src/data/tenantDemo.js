const mongoose = require('mongoose');

const users = [
    {
        _id: mongoose.Types.ObjectId('5d84e4396745ac3703655a6a'),
        accounts: [
            '5d9f5ee23fedbb62d55f8a16',
        ],
        email: 'demo@arrowsoluciones.com.ar',
        firstName: 'demo',
        lastName: 'client',
        picture: 'https://lh3.googleusercontent.com/-FBY2TSzSpZs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckwJyJBF_b1-Usfd3D-gup2q7w1kw/s96-c/photo.jpg',
        locations: [],
        devices: [
            {
                locale: 'en',
                _id: mongoose.Types.ObjectId('5ea280da543f34595faf2506'),
                id: 'Lucas\'s iPhone',
                description: 'Lucas\'s iPhone',
                token: 'ExponentPushToken[5ivE_5OqJ1Lr2KG_NCT-k7]',
            },
            {
                locale: 'en',
                _id: mongoose.Types.ObjectId('5ecd800085d66e3150289966'),
                id: 'Pablo’s iPhone',
                description: 'Pablo’s iPhone',
                token: 'ExponentPushToken[uSxbkCA7zPw2ye1z41gtuX]',
            },
        ],
        __v: 0,
        accountId: '5d84e4396745ac3703655a6b',
        business: 'NaN digital',
        fullName: 'Test',
        phone: '3513086184',
        position: 'Ingeniero',
        updatedAt: '2020-11-11T20:08:32.273Z',
        subscription: 'SUBS',
    },
];
module.exports = {
    users,
};
