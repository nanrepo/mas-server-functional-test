const mongoose = require('mongoose');

const users = [
  {
    _id: mongoose.Types.ObjectId('5d5c612bc7ddf4092ea742a6'),
    roles: [],
    accounts: [],
    subscription: 'SUBS',
    email: 'info2@arrowsoluciones.com.ar',
    firstName: 'NaN',
    lastName: 'Digital',
    picture:
      'https://lh3.googleusercontent.com/a-/AOh14GjFwHzruuhp1dolwiSk-3wiadJaZtBJ72PUDbbA=s96-c',
    business: null,
    gmbAccount: true,
    locations: [],
    devices: [],
    createdAt: '2021-06-25T17:47:12.305Z',
    updatedAt: '2021-06-25T17:47:12.353Z',
    __v: 0,
    accountId: '60d616a0241e847b112e7470',
  },
];

module.exports = {
    users,
};
