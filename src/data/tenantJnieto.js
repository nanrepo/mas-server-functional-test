const mongoose = require('mongoose');

const users = [
  {
    _id: mongoose.Types.ObjectId( '60fb3d088f73ee617ca8c572'),
    roles: [],
    accounts: [mongoose.Types.ObjectId( '60d61543241e847b112e7468')],
    subscription: 'SUBS',
    email: 'info@arrowsoluciones.com.ar',
    firstName: 'Juan',
    lastName: 'Nieto',
    picture:
      'https://lh3.googleusercontent.com/a/AATXAJzJrLU9yyMHsJquQAIqoCJuziGZgrfe5N4j0r2M=s96-c',
    business: null,
    gmbAccount: true,
    locations: [],
    devices: [],
    createdAt: '2021-06-25T17:41:23.102Z',
    updatedAt: '2021-06-25T17:44:59.403Z',
    __v: 0,
    accountId: '60d61543241e847b112e7468',
  },
];


module.exports = {
    users,
};
